# Changelog

## [0.1.1](https://gitea.com/gitea/changelog/pulls?q=&type=all&state=closed&milestone=1236) - 2020-02-02
* BUGFIXES
  * Use .changelog.yml config if in working dir (#37)
  * Fix Gitea get closed milestone too (#35)

## [0.1.0](https://gitea.com/gitea/changelog/pulls?q=&type=all&state=closed&milestone=1231) - 2020-01-25
* FEATURES
  * Add init command (#33)
  * Add subcommand to display contributors of milestone (#10)
* BUGFIXES
  * Fix README about import path (#22)
* ENHANCEMENTS
  * Changelog Overhaul 2 (#19)
  * Changelog Overhaul (#18)
* BUILD
  * CI and Linter (#27)
  * Fix CI (GOPROXY) (#26)
  * Add drone file (#23)
